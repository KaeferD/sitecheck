<?php 
require 'vendor/autoload.php';

require 'simple_html_dom.php';

use \Spatie\Crawler\Crawler;
use \Spatie\Crawler\CrawlObserver;
use \Spatie\Crawler\CrawlInternalUrls;
use \Spatie\Crawler\Url;


class MyCrawlObserver implements CrawlObserver
{
    private $urls = array();

    /**
     * Called when the crawler will crawl the url.
     *
     * @param \Spatie\Crawler\Url $url
     *
     * @return void
     */
    public function willCrawl(Url $url){

    }

    /**
     * Called when the crawler has crawled the given url.
     *
     * @param \Spatie\Crawler\Url $url
     * @param \Psr\Http\Message\ResponseInterface|null $response
     * @param \Spatie\Crawler\Url $foundOnUrl
     *
     * @return void
     */
    public function hasBeenCrawled(Url $url, $response, Url $foundOnUrl = null){
        if( is_null($response) ) return;

        $item = [
            'href' => (string) $url,
            'http_status' => $response->getStatusCode(),
            'title' => $this->page_title($response->getBody()),
            'foundOnUrl' => (string) $foundOnUrl
        ];

        $page = new \Page(
            (string) $url, 
            (string) $response->getBody(),
            (string) $foundOnUrl
        );

        $page->proccess();

        $this->urls[] = $item;
    }
    /**
     * Called when the crawl has ended.
     *
     * @return void
     */
    public function finishedCrawling(){
        #var_dump($this->urls);
    }

    private function page_title($body){
        $res = preg_match("/<title>(.*)<\/title>/siU", $body, $title_matches);
        if (!$res) 
            return null; 

        // Clean up title: remove EOL's and excessive whitespace.
        $title = preg_replace('/\s+/', ' ', $title_matches[1]);
        $title = trim($title);
        return $title;
    }
}

$url = 'http://feuerwehr-mindelheim.de/';

Crawler::create()
    ->setCrawlObserver(new MyCrawlObserver())
    ->setCrawlProfile(new CrawlInternalUrls($url))
    ->setConcurrency(1)
    ->setMaximumCrawlCount(2) 
    ->startCrawling($url);


class Page {

    public function __construct($url, $source = null, $foundOnUrl = null){
        $this->url = $url;

        $this->source = $source;

        $this->parser = new simple_html_dom();
    }

    public function proccess(){

        if($this->source == null){
            throw new \Exeption('No page html source has been set!');
        }

        #var_dump($this->source);

        // Load HTML from a string
        $this->parser->load($this->source);

        $this->extractMeta();
        $this->extractLinks();
        $this->extractHeadlines();
    }

    public function extractLinks(){
        $raw_links = $this->parser->find('a');

        $links = array();
        foreach ($raw_links as $link) {
            $links[] = [
                'url' => $link->href,
                'text' => $link->plaintext,
                'attributes' => $link->getAllAttributes()
            ];
        }

        #var_dump($links);
    }

    public function extractHeadlines(){
        $headlines = array();

        foreach (['h1', 'h2', 'h3', 'h4', 'h5', 'h6'] as $heading_tag) {

            $raw_heading = $this->parser->find($heading_tag);

            foreach ($raw_heading as $heading) {
                $headlines[$heading_tag][] = [
                    'text' => $heading->plaintext,
                    'attributes' => $heading->getAllAttributes()
                ];
            }

        }

        var_dump($headlines);
    }

    public function extractMeta(){
        $title = $this->parser->find('title')[0]->plaintext;

        $description = $this->parser->find('meta[name="description"]');
        if(is_array($description) && count($description) > 0){
            $description = $description[0]->getAttribute('content');
        }else{
            $description = "";
        }


        $canonical = $this->parser->find('link[rel="canonical"]');
        if(is_array($canonical) && count($canonical) > 0){
            $canonical = $canonical[0]->getAttribute('href');
        }else{
            $canonical = "";
        }

        #$wordcount = str_word_count($this->parser->find('body')[0]->plaintext, 2, '0..9ÄäÖöÜüß');

        #$wordcount = sanitize_words($this->parser->find('body')[0]->plaintext);

        $wordcount = get_num_of_words($this->parser->find('body')[0]->plaintext);

        var_dump($title);
        var_dump($description);
        var_dump($canonical);
        var_dump($wordcount);

    }

}

function sanitize_words($string) {
    preg_match_all("/\p{L}[\p{L}\p{Mn}\p{Pd}'\x{2019}]*/u",$string,$matches,PREG_PATTERN_ORDER);
    return $matches[0];
}

function get_num_of_words($string) {
        $string = preg_replace('/\s+/', ' ', trim($string));
        $words = explode(" ", $string);
        return count($words);
    }